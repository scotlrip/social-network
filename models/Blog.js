let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let blogSchema = new Schema({
    author:{
        type:Schema.Types.ObjectId,
        ref:'user'
    },
    content:String,
    createAt:{
        type:Date,
        default:Date.now
    }
});

let BlogModel = mongoose.model('blog',blogSchema);
exports.BLOG_MODEL = BlogModel;